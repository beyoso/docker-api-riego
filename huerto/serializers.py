from rest_framework import serializers
from .models import Huerto
"""
class HuertoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Huerto
        fields = ('id', 'name', 'date', 'temperatura', 'humedadSuelo','humedadAmbiente')
"""
class HuertoSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    date = serializers.DateTimeField(read_only=True)
    temperatura = serializers.IntegerField()
    humedadSuelo = serializers.IntegerField()
    humedadAmbiente = serializers.IntegerField()
    
    

    def create(self, validated_data):
        """
        Create and return a new `Serie` instance, given the validated data.
        """
        return Huerto.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Serie` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.temperatura = validated_data.get('temperatura', instance.temperatura)
        instance.humedadSuelo = validated_data.get('humedadSuelo', instance.humedadSuelo)
        instance.humedadAmbiente = validated_data.get('humedadAmbiente', instance.humedadAmbiente)
        instance.save()
        return instance
