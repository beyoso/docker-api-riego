# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import django
import time
import json
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from huerto.models import Huerto
from huerto.serializers import HuertoSerializer
# Create your views here.

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)
        
@csrf_exempt
def huerto_list(request):
    """
    List all code serie, or create a new serie.
    """
    if request.method == 'GET':
        huerto = Huerto.objects.all()
        serializer = HuertoSerializer(huerto, many=True)
        return JSONResponse(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = HuertoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)
@csrf_exempt
def huerto_detail(request, pk):
    """
    Retrieve, update or delete a serie.
    """
    try:
        huerto = Huerto.objects.get(pk=pk)
    except Huerto.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = HuertoSerializer(huerto)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = HuertoSerializer(huerto, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        huerto.delete()
        return HttpResponse(status=204)

@csrf_exempt
def index(request):
    """
    Función vista para la página inicio del sitio.
    """
    # Genera contadores de algunos de los objetos principales
    num_muestras = Huerto.objects.count()
    #datos=Huerto.temperatura().count()
    #temperaturaMedia=Huerto.objects.all().annotate(totalTemp=sum('temperatura'))
    temp=(Huerto.objects.get(pk=num_muestras)).temperatura
    humA=(Huerto.objects.get(pk=num_muestras)).humedadAmbiente
    humS=(Huerto.objects.get(pk=num_muestras)).humedadSuelo
    context={'id':"InvernaderoCasa",'datosGrafica':getDatosTempHum(),'mediaTemp':mediaTemp(),'mediaHumAmbiente':mediaHumAmbiente(),'mediaHumSuelo':mediaHumSuelo(),
    'temp':temp,'humA':humA,'humS':humS
    }
    
    return render(request,'index.html',context)
    #return HttpResponse("django Version: "+str(django.get_version())+"<p>Numero de muestras: "+str(num_muestras)  + "<p> Temperatura media: %.2f "%mediaTemp+"<p>Media hum= %.2f"%mediaHum)
    """
    # Renderiza la plantilla HTML index.html con los datos en la variable contexto
    return render(
        request,
        'index.html',
        context={'num_books':num_books,'num_instances':num_instances,'num_instances_available':num_instances_available,'num_authors':num_authors},
    )
    """
@csrf_exempt
def admin(request):
    print('hola')

def mediaTemp():
    totalTemp =0
    cont=0
    for i in Huerto.objects.all():
        if(int(i.date.strftime("%H"))>=int(time.strftime("%H"))-3 and (i.date.strftime("%j")==time.strftime("%j")) and (i.date.strftime("%y")==time.strftime("%y"))):
            cont+=1
            totalTemp+=int(i.temperatura)  
    mediaTemp= round(totalTemp/cont,2)
    return mediaTemp
def mediaHumAmbiente():
    totalHum =0
    cont=0
    for i in Huerto.objects.all():
        if(int(i.date.strftime("%H"))>=int(time.strftime("%H"))-3 and (i.date.strftime("%j")==time.strftime("%j"))and (i.date.strftime("%y")==time.strftime("%y"))):
            cont+=1
            totalHum+=int(i.humedadAmbiente)  
    mediaHumAmbiente= round(totalHum/cont,2)
    return mediaHumAmbiente
def mediaHumSuelo():
    totalHum =0
    cont=0
    for i in Huerto.objects.all():
        if(int(i.date.strftime("%H"))>=int(time.strftime("%H"))-3 and (i.date.strftime("%j")==time.strftime("%j"))and (i.date.strftime("%y")==time.strftime("%y"))):
            cont+=1
            totalHum+=int(i.humedadSuelo)  
    mediaHumSuelo= round(totalHum/cont,2)
    return mediaHumSuelo
def getDatosTempHum():
    cont = Huerto.objects.all().count()
    datos = ''
    pasos = int(cont/10)
    for i in Huerto.objects.all():
        temp = i.temperatura
        humA = i.humedadAmbiente
        date = i.date.strftime("%j")
        datos += ('['+str(date)+','+str(temp)+','+str(humA)+'],')
    return datos 
    
