from django.conf.urls import url
from huerto import views


urlpatterns = [
    url(r'index/',views.index,name='index'),
    url(r'admin/$', views.admin, name="admin"),
    url(r'^huerto/$', views.huerto_list),
    url(r'^huerto/(?P<pk>[0-9]+)/$', views.huerto_detail),
]
